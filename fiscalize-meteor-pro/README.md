# Fiscalize Web Admin

Meteor app para fiscalização das notas fiscais marcadas como suspeita nos aplicativos.

## Instalação

* Instale meteor: https://www.meteor.com/install
* Clone este projeto: `git clone git@github.com:danilo-favoratti/fiscalize-web-admin.git`
* Acesse o diretório deste projeto: `cd fiscalize-web-admin`
* Execute o servidor: `meteor --settings settings.json`
* Acesse: [http://localhost:3000](http://localhost:3000)

## Autor

Danilo Favoratti de Mendonça - favoratti@gmail.com