//
//  AppDelegate.h
//  fiscalize-ios
//
//  Created by Jhonatan Richard Raphael on 5/8/15.
//  Copyright (c) 2015 OPS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

