//
//  main.m
//  fiscalize-ios
//
//  Created by Jhonatan Richard Raphael on 5/8/15.
//  Copyright (c) 2015 OPS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
